﻿using UnityEngine;

public class BaseScript : MonoBehaviour
{
    protected static Transform CameraTF { get; set; }
    protected static Character AI { get; set; }
    protected Character Hero { get; set; }
}