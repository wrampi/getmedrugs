﻿using UnityEngine;

public class AIRadar : MonoBehaviour
{
    AI AI;

    void Awake() => AI = transform.parent.GetComponent<AI>();

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == Const.PlatformTag)
            if (!AI.Platforms.Contains(target.transform))
                AI.Platforms.Add(target.transform);

        if (target.tag == Const.DangerTag)
            AI.Danger = target.transform;

        if (target.tag == Const.CharacterTag)
            AI.Target = target.transform;
    }

    void OnTriggerExit2D(Collider2D target)
    {
        if (target.tag == Const.PlatformTag)
            if (AI.Platforms.Contains(target.transform))
                AI.Platforms.Remove(target.transform);

        if (target.tag == Const.DangerTag)
            AI.Danger = null;

        if (target.tag == Const.CharacterTag)
            AI.Target = null;
    }
}
