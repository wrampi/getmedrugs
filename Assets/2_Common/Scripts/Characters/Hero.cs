﻿using System.Collections;
using UnityEngine;

public class Hero : Character {

    public Light Sky, Light;
    Transform LightTF;

    public UnityEngine.UI.Text Text;
    GameObject FalloutEffect, HangedEffect;

    float xAxis;

    Vector3 lowPassValue;

    void Start()
    {
        Catcher = Settings.CatcherHero;
        lowPassValue = Input.acceleration;
        FalloutEffect = TF.parent.GetChild(4).gameObject;
        HangedEffect = TF.parent.GetChild(5).gameObject;
        LightTF = Light.transform;
    }

    protected override void FixedUpdate()
    {
        Light.range = Settings.World.Light * (1 - Environment.NowIntensity * .01f);
        LightTF.position = new Vector3(LightTF.position.x, LightTF.position.y, -Light.range * .5f);
        Sky.range = Light.range + 10;
        Sky.intensity = Sky.range * .02f;
        //Unfinished они должны быть связаны с погодой

        if (isJump)
        //запрет перемещения
        {
            if (Application.platform == RuntimePlatform.Android)
                xAxis = Correct(Mathf.Ceil(Settings.Way * Input.acceleration.x * 10) * .1f);
            else
                xAxis = Settings.Way * Input.GetAxis("Horizontal");
            rb.AddForce(new Vector2(xAxis * (Settings.World.Acceleration + Settings.Acceleration), 0), ForceMode2D.Force);
        }

        base.FixedUpdate();

        if (isStun)
        //сокращение времени ожидания
        {
            Vector3 acceleration = Input.acceleration;
            lowPassValue = Vector3.Lerp(lowPassValue, acceleration, .01f);
            Vector3 deltaAcceleration = acceleration - lowPassValue;
            Text.text = (Stuntime).ToString();

            if (deltaAcceleration.sqrMagnitude >= 25)
            {
                VibratorWrapper.Vibrate(200L);
                Stuntime -= 1 + StunValue;
            }
        }

        if (HangedEffect.activeSelf && Stuntime < 10)
            StartCoroutine(CrackOff(HangedEffect));
    }

    protected override void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == Const.DangerTag)
        {
            FalloutEffect.SetActive(true);
            StartCoroutine(CrackOff(FalloutEffect));
            target.gameObject.SetActive(false);
        }

        base.OnTriggerEnter2D(target);
    }

    protected override void OnCollisionEnter2D(Collision2D target)
    {
        if (target.collider.tag == Const.HangedTag && rb.velocity.y <= Const.TreshHoldVelocity)
            HangedEffect.SetActive(true);            

        base.OnCollisionEnter2D(target);        
    }

    IEnumerator CrackOff(GameObject gameObject)
    {
        yield return new WaitForSeconds(Const.One);
        gameObject.SetActive(false);
    }
}

public class Character : BaseScript
{
    #region Parameters
    //cache
    Animator animator;
    AudioSource audioSource;
    protected Rigidbody2D rb;
    SpriteRenderer Sprite;
    internal Rope Rope;
    protected Transform TF;
    protected TargetJoint2D Joint;

    //main triggers
    internal bool isJump = true;           //прыгает
    protected bool isStun = false;          //ошеломлен
    protected bool isAdhered = false;       //удерживается
    public bool haveRope = true;            //есть паутинка

    //data
    public bool Catcher = false;
    float defX;
    internal int jump;
    protected Vector2? LastCollision;
    protected byte wallCatch;
    //
    protected float StunValue;
    float stuntime;             //а я могу хранить в той переменной?
    protected float Stuntime
    {
        get => stuntime;
        set
        {
            stuntime = value;
            if (stuntime > 100)
                stuntime = 100;
        }
    }
    //
    Vector3 NeedAngle, ThisAngle;
    bool NeedTurn = true;

    //coroutines
    CoroutineObject StunRoutine;
    CoroutineObject TurnRoutine;
    #endregion
    #region Unity Methods

    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
        Sprite = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
        StunRoutine = new CoroutineObject(this, Stun);
        TurnRoutine = new CoroutineObject(this, Turn);

        TF = transform;
        Rope = TF.parent.GetChild(0).GetChild(0).GetComponent<Rope>();
        defX = TF.position.x;      
        Joint = Rope.Body;
        rb = Joint.attachedRigidbody;

        Set();
        //rb
    }

    protected virtual void FixedUpdate()
    {
        if (LastCollision != null && isJump)
        //после разрешения начинаем прыгать, если есть косание с платформой
        {
            PlayView(Const.AnimJump, Settings.World.JumpOGG);
            Impulse((Vector2)LastCollision);
            LastCollision = null;
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == Const.PlatformTag)
            TurnTF(target.transform.eulerAngles.z);
        else if (target.tag == Const.DangerTag)
        {
            //Unfinished звук для другой темы, анимация стана
            PlayView(Const.AnimNo, Settings.World.FalloutOGG, 0.1f);
            Impulse(Vector3.down);
            OnStun();
            Rope.StopRope();
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D target)
    {
        if (target.tag == Const.PlatformTag)
            TurnTF();
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == Const.CharacterTag)
        {
            Catch();
            return;
        }

        if (collision.gameObject.tag == Const.HangedTag && rb.velocity.y < Const.TreshHoldVelocity && !isAdhered)
        {
            //Unfinished для другой темы ничего нет (делегаты?)
            LastCollision = Platform(collision.transform.rotation.z);

            PlayView(Const.AnimNo, Settings.World.HangedsOGG[Settings.Theme]);
            Impulse(Vector3.zero);
            OnStun();

            Rope.StopRope();
            isAdhered = Joint.enabled = true;
            Joint.target = TF.position;
        }

        if (!isJump)
        {
            if (!isAdhered)
            {
                PlayView(Const.AnimNo, Settings.World.JumpOGG, 0.1f);
                isAdhered = Joint.enabled = true;
                Joint.target = TF.position;
                if (collision.gameObject.tag == Const.WallTag)
                    LastCollision = Wall(collision.transform.position.x);
                else
                    LastCollision = Platform(collision.transform.rotation.z);
            }
            if (isStun)
                //Unfinished анимация стана
                PlayView(Const.AnimNo, null);
            return;
        }

        if (Joint.enabled)//Unfinished рентабельность есть, но проверить надо
            Rope.StopRope();

        if (collision.gameObject.tag == Const.PlatformTag)
            if (rb.velocity.y < Const.TreshHoldVelocity)
            {

                PlayView(Const.AnimJump, Settings.World.JumpOGG);
                Impulse(Platform(collision.transform.rotation.z));
            }

        if (collision.gameObject.tag == Const.WallTag)
        {
            PlayView(Const.AnimJumpOfWall, Settings.World.JumpOGG);
            Impulse(Wall(collision.transform.position.x));
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (!isJump && collision.collider.tag == Const.PlatformTag)
            LastCollision = null;
    }
    #endregion
    #region Physic Methods

    /// <summary>
    /// Physic Methods
    /// <para>Get <see cref="rb"/> impulse to vector</para>
    /// </summary>
    void Impulse(Vector2 vector)
    {
        rb.velocity = Vector2.zero;
        rb.AddForce(vector, ForceMode2D.Impulse);
    }

    /// <summary>Physic Methods</summary>
    Vector2 Platform(float angle)
    {
        wallCatch = Const.Zero;
        return new Vector2(-Mathf.Sin(angle), Mathf.Cos(angle)) * jump;
    }

    /// <summary>Physic Methods</summary>
    Vector2 Wall(float posX)
    {
        Vector2 vector = new Vector2(14, jump - wallCatch++ * 7);
        if (posX - CameraTF.position.x > Const.Zero)
        {
            vector.x = -vector.x;
            Sprite.flipX = true;
        }
        return vector;
    }
    #endregion
    #region Controller Methods

    /// <summary>Controller Methods</summary>
    void TurnTF(float angle = 0)
    {
        TurnRoutine.Stop();
        NeedAngle.z = angle > 180 ? angle - 360 : angle;
        ThisAngle = TF.eulerAngles;
        if (ThisAngle.z > 180)
            ThisAngle.z -= 360;
        if (NeedTurn)
            TurnRoutine.Start();
    }

    //функции для Rope при начале и завершении его событий
    internal void ActiveRope(float angle)
    {
        NeedTurn = false;
        TurnRoutine.Stop();
        TF.eulerAngles = new Vector3(0, 0, angle);
    }
    //
    internal void DisableRope()
    {
        PlayView(Const.AnimNo, null);
        NeedTurn = true;
        TurnRoutine.Start();
    }

    public void Catch()
    {
        Rope.StopRope();
        rb.bodyType = RigidbodyType2D.Kinematic;
        rb.velocity = Vector2.zero;
        enabled = false;

        StartCoroutine(WaitGameOver());

        if (Catcher)
        {
            //анимация ловли
        } else
        {
            //анимация пойманного
        }
    }

    /// <summary>Controller Methods</summary>
    void OnStun()
    {
        if (isStun)
            Stuntime += 20 - StunValue;
        else
            StunRoutine.Start();
    }

    IEnumerator Turn()
    {
        float time = .5f;
        if (NeedAngle.z == 0)
            time = .1f;
        while (ThisAngle != NeedAngle)
        {
            TF.eulerAngles = ThisAngle = Vector3.Lerp(ThisAngle, NeedAngle, time);
            yield return null;
        }
    }

    IEnumerator Stun()
    {
        isStun = true;
        isJump = false;
        Stuntime = 50 - StunValue * 10;

        while (Stuntime-- > 0)
            yield return new WaitForSeconds(.1f);
        isStun = isAdhered = Joint.enabled = false;
        isJump = true;
        PlayView(Const.AnimNo, null);
    }

    IEnumerator WaitGameOver()
    {
        //Unfinished
        yield return new WaitForSeconds(2);
        rb.position = new Vector2(defX, 0);

        PlayView(Const.AnimNo, null);

        rb.velocity = Vector2.zero;
        rb.bodyType = RigidbodyType2D.Dynamic;
        if (Settings.Game)
            Settings.Game = false;
    }
    #endregion
    #region Another Methods

    /// <summary>Another Methods</summary>
    void Set()
    {
        animator.runtimeAnimatorController = Settings.World.BodyAnim;
        StunValue = Settings.StunTime * .1f;
        jump = Settings.World.Jump + Settings.Jump;
    }

    /// <summary>Another Methods</summary>
    internal void PlayView(int animation, AudioClip audioClip, float volume = 1)
    {
        animator.SetInteger("State", animation);

        if (Settings.GetSound())
            audioSource.PlayOneShot(audioClip, volume);
    }

    /// <summary>Another Methods</summary>
    public void NoAnim()
    {
        Sprite.flipX = false;
        PlayView(Const.AnimNo, null);
    }

    /// <summary>Another Methods</summary>
    protected float Correct(float i) => i >= .6f ? 1.2f : i <= -.6f ? -1.2f : i * 2;
    #endregion
}
/*{
    public bool Catcher = false;
    float defX;
    protected byte WallCatch = Const.Zero;

    SpriteRenderer Sprite;
    Animator MainAnimator;
    AudioSource mainAudio;

    //cache parameters
    internal Rope Rope;
    protected Transform CameraTF, TF;    
    protected TargetJoint2D Joint;
    protected Rigidbody2D RB;

    //main triggers
    public bool isJump = true;          //прыгает
    protected bool isStun = false;      //ошеломлен
    protected bool isAdhered = false;   //удерживается
    public bool haveRope = true;     //есть паутинка

    protected virtual void Awake()
    {
        CameraTF = Camera.main.transform;
        TF = transform;
        Rope = TF.parent.GetChild(0).GetChild(0).GetComponent<Rope>();
        defX = TF.position.x;
        MainAnimator = GetComponent<Animator>();
        Sprite = GetComponent<SpriteRenderer>();
        mainAudio = GetComponent<AudioSource>();
        
        Joint = Rope.Body;
        RB = Joint.attachedRigidbody;

        StunAwake();
        TurnAwake();
        Set();
    }

    protected virtual void FixedUpdate() => JumpUpdate();

    protected float Correct(float i) => i >= .6f ? 1.2f : i <= -.6f ? -1.2f : i * 2;

    public void Animation(int index) => MainAnimator.SetInteger("State", index);

    public void NoAnim()
    {
        Sprite.flipX = false;
        Animation(Const.AnimNo);
    }

    public void Set()
    {
        MainAnimator.runtimeAnimatorController = Settings.World.BodyAnim;
        JumpSet();
        StunSet();
    }

    protected virtual void OnTriggerEnter2D(Collider2D target)
    {
        TurnTrigPlatform(target.tag, target.transform.eulerAngles.z);
        DangersEnter(target.gameObject.tag);
    }

    protected virtual void OnTriggerExit2D(Collider2D target) => TurnTrigPlatform(target.tag);

    protected virtual void OnCollisionEnter2D(Collision2D target)
    {
        if (target.collider.tag == Const.CharacterTag)
        {
            Catch();
            return;
        }
        if (HangedEnter(target))//проверка на косание Hanged-платформы
            return;
        if (!NoJumpEnter(target))//проверка NoJump
            return;

        if (Joint.enabled)//Unfinished рентабельность есть, но проверить надо
            Rope.StopRope();

        PlatformEnter(target);//проверка на косание с платформой
        WallEnter(target);//проверка на косание со стеной
    }

    void OnCollisionExit2D(Collision2D target)
    {
        if (!isJump && target.collider.tag == Const.PlatformTag)
            Last = null;
    }

    void Impulse(Vector3 vector)
    {
        RB.velocity = Vector3.zero;
        RB.AddForce(vector, ForceMode2D.Impulse);
    }

    public void Catch()
    {
        Rope.StopRope();
        RB.bodyType = RigidbodyType2D.Kinematic;
        RB.velocity = Vector2.zero;        
        enabled = false;

        StartCoroutine(WaitGameOver());

        if (Catcher)
        {
            //анимация ловли
        }else
        {
            //анимация пойманного
        }        
    }

    IEnumerator WaitGameOver()
    {
        yield return new WaitForSeconds(2);
        RB.position = new Vector2(defX, 0);
        
        Animation(Const.AnimNo);

        RB.velocity = Vector2.zero;
        RB.bodyType = RigidbodyType2D.Dynamic;
        if (Settings.Game)
            Settings.Game = false;
    }

    #region Jump
    internal int Jump;    
    protected Transform Last;

    void JumpSet() => Jump = Settings.World.Jump + Settings.Jump;

    void JumpUpdate()
    {
        if (Last != null && isJump)
        //после разрешения начинаем прыгать, если есть косание с платформой
        {
            WallCatch = Const.Zero;

            Impulse(new Vector2(-Mathf.Sin(Last.rotation.z) * Jump,
                Mathf.Cos(Last.rotation.z) * Jump));

            Animation(Const.AnimJump);

            Last = null;
        }       
    }

    bool NoJumpEnter(Collision2D target)
    {
        if (!isJump)
        //проверять только касание с платформами и стеной
        {
            if (Settings.Sound)
                mainAudio.PlayOneShot(Settings.World.JumpOGG, 0.1f);
            if (target.gameObject.tag != Const.WallTag)
                //но не отталкиваться от стены
                Last = target.transform;
        }
        return isJump;
    }

    void PlatformEnter(Collision2D target)
    {
        if (target.gameObject.tag == Const.PlatformTag && RB.velocity.y <= Const.TreshHoldVelocity)
        {
            if (Settings.Sound)
                mainAudio.PlayOneShot(Settings.World.JumpOGG);

            WallCatch = Const.Zero;

            Impulse(new Vector2(-Mathf.Sin(target.transform.rotation.z) * Jump,
                Mathf.Cos(target.transform.rotation.z) * Jump));

            Animation(Const.AnimJump);
        }
    }

    void WallEnter(Collision2D target)
    {
        if (target.gameObject.tag == Const.WallTag)
        {
            if (Settings.Sound)
                mainAudio.PlayOneShot(Settings.World.JumpOGG);

            Vector3 vector = new Vector3(14, Jump - WallCatch++ * 7);
            if (target.transform.position.x - CameraTF.position.x > Const.Zero)
            {
                vector.x = -vector.x;
                Sprite.flipX = true;
            }

            Impulse(vector);

            Animation(Const.AnimJumpOfWall);
        }
    }
    #endregion
    #region Turn
    Vector3 NeedAngle, ThisAngle;
    bool NeedTurn = true;

    CoroutineObject TurnRoutine;

    void TurnAwake() => TurnRoutine = new CoroutineObject(this, Turn);

    void TurnTrigPlatform(string tag, float angle = 0)
    {
        if (tag == Const.PlatformTag)
        {
            TurnRoutine.Stop();
            NeedAngle.z = angle > 180 ? angle - 360 : angle;
            ThisAngle = TF.eulerAngles;
            if (ThisAngle.z > 180)
                ThisAngle.z -= 360;
            if (NeedTurn)
                TurnRoutine.Start();
        }
    }
    
    //функции для Rope при начале и завершении его событий
    public void ActiveRope(float angle)
    {
        NeedTurn = false;
        TF.eulerAngles = new Vector3(0, 0, angle);
    }
    //
    public void DisableRope()
    {
        Animation(Const.AnimNo);
        NeedTurn = true;
        TurnRoutine.Start();
    }

    IEnumerator Turn()
    {
        float time = .5f;
        if (NeedAngle.z == Const.Zero)
            time = .1f;
        while (NeedTurn && ThisAngle != NeedAngle)
        {
            TF.eulerAngles = ThisAngle = Vector3.Lerp(ThisAngle, NeedAngle, time);
            yield return null;
        }
    }
    #endregion
    #region Stun    
    protected float StunValue;
    float stuntime;//а я могу хранить в той переменной
    protected float Stuntime
    {
        get => stuntime;
        set
        {
            stuntime = value;
            if (stuntime > 100)
                stuntime = 100;
        }
    }
    CoroutineObject StunRoutine;

    void StunAwake() => StunRoutine = new CoroutineObject(this, Stun);
    void StunSet() => StunValue = Settings.StunTime * .1f;

    bool HangedEnter(Collision2D target)
    {
        if (target.gameObject.tag == Const.HangedTag && RB.velocity.y <= Const.TreshHoldVelocity && !Joint.enabled)
        {//тут все не доконца верно
            //Rope.Body.enabled указывает на то, что джоинт активен, а не на то что мы застряли
            //и вообще для другой темы ничего нет (делегаты?)
            //Unfinished
            Last = target.transform;
            if (Settings.Sound)
                mainAudio.PlayOneShot(Settings.World.HangedsOGG[Settings.Theme]);

            Joint.enabled = true;
            Joint.target = TF.position;

            if (isStun)
            {
                Stuntime += 20 - StunValue;
                return true;
            }
            Impulse(Vector3.zero);
            StunRoutine.Start();
        }
        return false;
    }

    void DangersEnter(string tag)
    {
        if (tag == Const.DangerTag)
        {
            //Unfinished звук для другой темы
            if (Settings.Sound)
                mainAudio.PlayOneShot(Settings.World.FalloutOGG, 1f);
            if (!isJump)
            {
                if (isStun)
                    Stuntime += 20 - StunValue;
                else
                    StunRoutine.Start();
                return;
            }
            Impulse(Vector3.down);//Unfinished это вообще надо?
            StunRoutine.Start();
        }
    }

    IEnumerator Stun()
    {
        isStun = true;
        isJump = false;
        Stuntime = 50 - StunValue * 10;
        //Unfinished анимация стана

        while (Stuntime-- > Const.Zero)
            yield return new WaitForSeconds(.1f);
        isStun = Joint.enabled = false;
        isJump = true;
    }
    #endregion
}*/
