﻿using UnityEngine;

internal class Rope : Link
{
    internal TargetJoint2D Body;
    int Last;

    Link[] Nudes = new Link[25];
    Vector2 velocity;
    bool Active;

    Character characterCS;

    protected override void Awake()
    {
        base.Awake();

        Transform buf = transform.parent;
        Body = buf.parent.GetComponent<TargetJoint2D>();
        characterCS = Body.gameObject.GetComponentInChildren<Character>();

        Nudes[0] = this;
        gameObject.SetActive(false);
        Nudes[1] = buf.GetChild(1).GetComponent<Link>();
        
        for (int i = 2; i < Nudes.Length; i++)
        {
            Nudes[i] = Instantiate(Nudes[1].gameObject, transform.parent).GetComponent<Link>();
            Nudes[i].gameObject.SetActive(false);
            Nudes[i].Hinge.connectedBody = Nudes[i - 1].Hinge.attachedRigidbody;
        }

        Nudes[1].gameObject.SetActive(false);

        Nudes[1].Hinge.connectedAnchor = Vector2.zero;
    }

    void FixedUpdate()
    {
        if (Active)
        {
            Hinge.attachedRigidbody.velocity = velocity + Body.attachedRigidbody.velocity; //Rope move

            if (Nudes[Last].transform.localPosition.magnitude > 1.5f)
            {   //active next Link
                Nudes[Last++].capsuleCollider.enabled = true;

                if (Last == Settings.World.LinkCount + Settings.LinkCount)//Unfinished длина у AI не достает при минимуме, длина для Menu
                {   //Rope too long
                    Active = false;
                    for (int i = 0; i < Nudes.Length; i++)
                        Nudes[i].capsuleCollider.enabled = false;
                    return;
                }

                Nudes[Last].transform.position = Nudes[Last - 1].NextPlace.transform.position;
                Nudes[Last].transform.eulerAngles = new Vector3(0, 0,
                    Nudes[Last].transform.localPosition.x > 0 ?
                    -Vector3.Angle(Vector3.up, Nudes[Last].transform.localPosition) :
                    Vector3.Angle(Vector3.up, Nudes[Last].transform.localPosition));

                Nudes[Last].gameObject.SetActive(true);
            }
        } else if (Body.enabled)
        {
            Body.target = Nudes[Last].NextPlace.transform.position; //Hero move

            if (Nudes[Last].capsuleCollider.enabled && Nudes[Last].transform.localPosition.magnitude < 3f)
                //Link so close
                Nudes[Last].capsuleCollider.enabled = false;

            if (Nudes[Last].transform.localPosition.magnitude < 2f)
            {   //Link grab 

                for (int i = 0; i < Last; i++)
                    Nudes[i].Hinge.attachedRigidbody.velocity = Vector2.zero;

                Nudes[Last--].gameObject.SetActive(false);

                Body.attachedRigidbody.velocity *= .1f; //reduction

                if (Last < 0)
                //Rope end
                {
                    Hinge.enabled = Body.enabled = false;
                    Body.attachedRigidbody.velocity = velocity.normalized * 20;
                    characterCS.DisableRope();
                    characterCS.haveRope = true;
                }
            }
        } else if (gameObject.transform.localPosition.magnitude > Nudes.Length * 2)
        //Rope's not on Camera
        {
            for (int i = 0; i < Nudes.Length; i++)
                Nudes[i].gameObject.SetActive(false);

            characterCS.haveRope = true;
        }
    }

    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == Const.PlatformTag || target.gameObject.tag == Const.WallTag
            || target.gameObject.tag == Const.HangedTag || (target.gameObject != Body.gameObject && target.collider.tag == Const.CharacterTag))
        {   //stop the Rope
            for (int i = 0; i < Nudes.Length; i++)
                Nudes[i].Hinge.attachedRigidbody.velocity = Vector2.zero;

            Hinge.connectedBody = target.rigidbody;
            Active = false;
            Hinge.enabled = Body.enabled = true;

            characterCS.PlayView(3, null);
        }
    }

    internal void OnDown() => GoRope(Camera.main.ScreenToWorldPoint(Input.mousePosition) - Body.gameObject.transform.position);

    public void OnUp() => characterCS.isJump = true;

    public void GoRope(Vector3 vector)
    {
        characterCS.isJump = false;
        if (StopRope())
        {
            characterCS.haveRope = false;
            Last = 0;
            gameObject.transform.localPosition = Vector3.zero;
            gameObject.SetActive(true);
            vector.z = 0;
            velocity = vector.normalized * 60;
            characterCS.ActiveRope(Vector2.SignedAngle(Vector2.up, velocity));
            Active = true;
        }
    }

    internal bool StopRope()
    {
        //Reset
        Hinge.enabled = false; ///wrong

        if (gameObject.activeSelf)
        //Rope's active
        {
            Active = false;
            for (int i = 0; i < Nudes.Length; i++)
                Nudes[i].capsuleCollider.enabled = false;
            if (Body.enabled)
            {
                Body.attachedRigidbody.velocity = velocity.normalized * 20;
                characterCS.DisableRope();
            }
            Body.enabled = false;
            return false;
        }
        Body.enabled = false;
        characterCS.haveRope = true;
        return true;
    }
}