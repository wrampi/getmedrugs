﻿using UnityEngine;

internal class Link : MonoBehaviour
{
    internal HingeJoint2D Hinge;
    internal GameObject NextPlace;
    internal CapsuleCollider2D capsuleCollider;

    protected virtual void Awake()
    {
        Hinge = GetComponent<HingeJoint2D>();
        NextPlace = transform.GetChild(0).gameObject;
        capsuleCollider = GetComponent<CapsuleCollider2D>();
    }
}