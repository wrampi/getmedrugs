﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : Character
{
    public bool isHero = false;

    Vector2 Next;

    internal List<Transform> Platforms = new List<Transform>();

    internal Transform Danger;
    internal Transform Target;

    public Transform ChoisePl;
    public Transform TopGO;
    CircleCollider2D MainCollider;

    //main triggers
    bool isRule, needWall = false, needRope = false;

    byte Near, value;
    float Top, TopWall, PrevTop;

    protected override void Awake()
    {
        base.Awake();
        AI = this;
        if (!isHero)
            Catcher = !Settings.CatcherHero;
        else
            Catcher = Settings.CatcherHero;
        MainCollider = GetComponent<CircleCollider2D>();

        if (isHero)
            go = true;
    }

    bool go = false, waiting = false;
    IEnumerator Wait()
    {
        waiting = true;
        yield return new WaitForSeconds(5);
        go = true;
        waiting = false;
    }

    protected override void FixedUpdate()
    {
        if (!go)
        {
            if (!waiting)
                StartCoroutine(Wait());
            return;
        }

        if (NoCamera())
            return;

        /*if (isJump && LastCollision != null && rb.velocity.y == 0)
            GetRealTop(LastCollision);*/

        base.FixedUpdate();

        if (Rope.gameObject.activeSelf)
            return;

        if (Danger != null  //убегаем от опасности
            &&
            Vector2.Distance(Danger.position, TF.position) < 10
            &&
            Danger.position.y > TF.position.y
            &&
            IsWithin(Danger))
        {
            if (haveRope && Random.Range(0, 15) == 0)  //complication
                RopeNeed(Danger.position.x - TF.position.x); //пробуем паутинкой
            else
                rb.AddForce(new Vector2(TF.position.x - Danger.position.x > 0 ? .6f : -.6f * 60, 0), ForceMode2D.Force); //complication
            return;
        }

        float RulePlf = 0;

        if (!needRope && !needWall && rb.velocity.y < 0 && (!isRule || (Near < Platforms.Count ? TF.position.y < Platforms[Near].position.y : true))) //падение
        {
            if (!ChoiseNearDown() || Vector2.Distance(Platforms[Near].position, TF.position) > 20 + value * 10) //complication
            {
                if (haveRope)
                    needRope = true; //пробуем паутинкой
                else if (wallCatch == 0)
                    needWall = true;
                isRule = false;
            }
        }

        if (Target != null) //оппонент рядом
        {
            Vector3 vector;
            if (haveRope)
            {
                if (Catcher)
                {
                    if (TF.position.y - Target.position.y > 5) //поймал анимация
                    {
                        PrevTop = 0;
                        jump -= value;
                        value = 0;
                        go = false;
                        Catch();
                        Target.GetComponent<Character>().Catch();
                        return;
                    }
                    vector = Target.position - TF.position;
                } else
                {
                    vector = TF.position - Target.position;
                    if (vector.y < 0)
                        vector.y = -vector.y;
                }

                Rope.GoRope(vector);
                wallCatch = 1;
                isJump = true;
                return;
            }
        }

        if (needRope && rb.velocity.y < 0)
            RopeNeed(TF.position.x);
        else if (needWall && rb.velocity.y < jump * .5f)//задержка
            RulePlf = TF.position.x - CameraTF.position.x > 0 ? .6f : -.6f;
        else
        {
            SetNext();
            RulePlf = Vector2.Distance(Next, TF.position);

            if (Target != null)
            {
                float buf = Vector2.Distance(Target.position, TF.position);
                if (buf < RulePlf)
                    RulePlf = Catcher ? buf : -buf;
            }

            if (RulePlf > 4)
                RulePlf = 2;

            RulePlf = Correct((Next.x - TF.position.x) * .075f * RulePlf);

            if (RulePlf < 0)
            {   //Left
                if (rb.velocity.x > 10)
                    rb.velocity *= .5f;
            } else if (rb.velocity.x < -10)
                rb.velocity *= .5f;
        }

        if (ChoisePl != null)
            ChoisePl.position = Next;
        //complication
        rb.AddForce(new Vector2(RulePlf * 60, 0), ForceMode2D.Force);
    }

    bool NoCamera()
    {
        float Pos = TF.position.y - CameraTF.position.y;
        if (Pos > -30 && Pos < 30)
        {
            if (!MainCollider.enabled)
            {
                rb.position = new Vector2(
                    CameraTF.position.x + Random.Range(-7, 8),
                    TF.parent.position.y);
                MainCollider.enabled = true;
                rb.bodyType = RigidbodyType2D.Dynamic;
                rb.AddForce(new Vector2(0, jump * 1.5f), ForceMode2D.Impulse);
            }
            return false;
        }

        //Unfinished complication
        if (MainCollider.enabled)
        {
            Rope.StopRope();
            MainCollider.enabled = false;
            rb.velocity = Vector3.zero;
            rb.bodyType = RigidbodyType2D.Kinematic;
        }

        if (Pos < 50)
            rb.position = new Vector2(CameraTF.position.x, rb.position.y + .1f);
        if (Pos < -50)
            rb.position = new Vector2(CameraTF.position.x, rb.position.y + .5f);

        return true;
    }

    bool ChoiseNearDown()
    {
        isRule = false;
        float buf, distance = 100;
        for (byte i = 0; i < Platforms.Count; i++)
            if (Platforms[i].position.y < TF.position.y //устанавливаем платформу ниже нас
                &&
                (buf = Vector2.Distance(Platforms[i].position, TF.position)) < distance //ближайшая
                &&
                IsAptIfLeaf(Platforms[i]))
            {
                distance = buf;
                Near = i;
                isRule = true;
            }
        return isRule;
    }

    protected override void OnCollisionEnter2D(Collision2D target)
    {
        if (target.collider.tag == Const.CharacterTag)
        {
            PrevTop = 0;
            jump -= value;
            value = 0;
            if (!isHero)
                go = false;
        }

        if (target.collider.tag == "Platform" && rb.velocity.y <= 3)
        {
            TopWall = 0;
            GetRealTop(target.transform);
            ChoisePlatform();
        }

        base.OnCollisionEnter2D(target);

        if (target.collider.tag == "Wall")
        {
            TopWall += jump * .5f - wallCatch * 3.5f;
            ChoisePlatform();
        }
    }

    void GetRealTop(Transform targetTF)
    {
        Top = Mathf.Cos(targetTF.rotation.z) * jump;
        Top *= Top;
        Top *= .0166f;
        Top += targetTF.position.y;
        Top -= (50 - Settings.LVL) * .1f;

        if (enabled)
            if (PrevTop < Top)
            {
                PrevTop = Top + 5;
                jump -= value;
                value = 0;
            } else if (value < 8)
            {
                value += 2;
                jump += 2;
            }
    }

    void ChoisePlatform()
    {
        isRule = needRope = needWall = false;

        if (TopGO != null)
            TopGO.position = new Vector2(CameraTF.position.x, Top + TopWall);

        if (haveRope && Random.Range(0, 100) < Settings.LVL + wallCatch * 10)
            //решил использовать Rope
            needRope = true;
        else if (wallCatch == 0 && Random.Range(0, 100) < 10 + Settings.LVL)
            //решил прыгать от стены
            needWall = true;
        else
        {   //выбор следующей платформы
            Near = 0;

            for (byte i = 0; i < Platforms.Count; i++)
                if (Platforms[i].position.y > TF.position.y
                &&
                Top + TopWall > Platforms[i].position.y
                &&
                Platforms[Near].position.y < Platforms[i].position.y
                &&
                IsAptIfLeaf(Platforms[i]))
                {
                    Near = i;
                    isRule = true;
                }

            if (!isRule && wallCatch == 0)
                //платформа не выбрана, пробуем от стены
                needWall = true;
        }
    }

    void SetNext()
    {
        if (Platforms.Count == 0)
            Next = CameraTF.position;
        else
        {
            if (Near >= Platforms.Count)
                Near = (byte)(Platforms.Count - 1);
            Next = Platforms[Near].position;
            if (Platforms[Near].name == "Near")
                Next.x += Platforms[Near].position.x - CameraTF.position.x > 0 ? -3 : 3;
        }
    }

    void RopeNeed(float x)
    {
        Rope.GoRope(Vector2.up + (x > 0 ? Vector2.left : Vector2.right));
        isJump = true;
        wallCatch = 1;
        needRope = false;
    }

    bool IsAptIfLeaf(Transform TF) => TF.name != "Leaf" || IsWithin(TF);

    bool IsWithin(Transform TF) 
        => TF.position.x - CameraTF.position.x > -8.47f
        && TF.position.x - CameraTF.position.x < 8.47f;
}