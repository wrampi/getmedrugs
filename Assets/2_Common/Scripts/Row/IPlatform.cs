﻿public interface IPlatform
{
    byte Enable();
    void Set();
    void HangsHere(UnityEngine.SpriteRenderer[] Patterns);    
}