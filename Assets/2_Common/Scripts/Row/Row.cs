﻿using UnityEngine;
using SR = UnityEngine.SpriteRenderer;

public class Row : MonoBehaviour
{
    public GameObject Stick;
    public static bool Hanged, Crack;
    IPlatform[] AllLines;
    SR[] Hangeds = new SR[5];

    void Awake()
    {
        Hangeds[0] = GetComponentInChildren<SR>();
        for (int i = 1; i < Hangeds.Length; i++)
            Hangeds[i] = Instantiate(Hangeds[0], Hangeds[0].transform.parent);

        Reconstruct();
    }
    
    public void Reconstruct()
    {
        IPlatform[] Platforms = GetComponentsInChildren<IPlatform>(true);

        byte CountStick = (byte)(Settings.World.PairOfSticks << 1);

        sbyte extra = (sbyte)(CountStick - Platforms.Length + 2);

        AllLines = new IPlatform[Platforms.Length + extra];

        if (extra > 0)
        {             
            for (int i = 0; i < extra; i++)
                AllLines[i] = Instantiate(Stick, transform).GetComponent<IPlatform>();

            for (int i = 0; i < Platforms.Length; i++)
                AllLines[extra + i] = Platforms[i];
        } else
            for (int i = 0; i < AllLines.Length; i++)
                AllLines[i] = Platforms[i];

        for (int i = 0; i < AllLines.Length; i++)
            AllLines[i].Set();
    }

    public void Enable()
    {
        Hangeds[0].transform.parent.gameObject.SetActive(false);

        gameObject.SetActive(false);
        gameObject.SetActive(true);

        if (transform.position.y == 0)
            return;

        Hanged = Crack = true;

        byte chek = 0;
        sbyte count = (sbyte)AllLines.Length;
        sbyte[] length = new sbyte[count];
        for (sbyte i = 0; i < length.Length; i++)
            length[i] = i;

        while (chek!=2)
        {
            if(count<=0)
            {
                Platform.global_y = (sbyte)(-RowKeeper.H * .5f);
                break;
            }
            byte buf = (byte)Random.Range(0, count--);
            while (length[buf] == -1)
                buf++;
            chek = AllLines[length[buf]].Enable();
            if (chek==1)
                AllLines[length[buf]].HangsHere(Hangeds);
            length[buf] = -1;
        }
    }

    void FixedUpdate()
    {
        float i = Camera.main.transform.position.y - transform.position.y;
        if (i < 9f && i > -9f)
            CameraTarget.x = transform.position.x;
    }
}