﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaf : MonoBehaviour {

    Rigidbody2D rb;

    SpriteRenderer MainSprite;

    void Awake ()
    {
        MainSprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
    }

    void OnEnable()
    {
        MainSprite.sprite = Settings.World.Leaves[Random.Range(0, Settings.World.Leaves.Length)];
        transform.localPosition = new Vector2(Random.Range(-6f, 6f), 17);
        rb.velocity = new Vector2(0, -1);
        StartCoroutine("Noise");
    }

    IEnumerator Noise()
    {
        int j = 0;
        while (true)
        {
            float i = Random.Range(0, .2f);

            if (j > 300)
                i = -i;

            if (j > 600)
                j = -1;
            j++;
            rb.velocity = new Vector2(i*10, rb.velocity.y);

            float angle = transform.eulerAngles.z;

            if(angle > 180)
            {
                angle = angle - 360;
                i = -i;
            }

            if (angle + i < 25 && angle + i > -25)
                transform.eulerAngles += new Vector3(0, 0, i);

            yield return null;
        }
    }
}
