﻿using Vector = UnityEngine.Vector3;
using Camera = UnityEngine.Camera;
using GameObj = UnityEngine.GameObject;
using Collision = UnityEngine.Collision2D;
using Collider = UnityEngine.CapsuleCollider2D;
using Joint = UnityEngine.TargetJoint2D;

public class Stick : Platform, IPlatform
{
    public static GameObj Crack;
    static float time = 0;

    Joint TargetJoint;
    Collider Collider;

    protected override void Awake()
    {
        //save values
        base.Awake();
        /* Renderers:
         * 0 - Platform(Stick)
         * 1 - Coating Platform
         * 2 - BackStick(Brench)
         * 3 - Coating Back
         * 4 - Crack Coating
         * Masks:
         * 0 - Platform(Stick)
         * 1 - Hanged
         * 2 - BackStick(Brench)
         */
        TargetJoint = GetComponent<Joint>();
        Collider = GetComponent<Collider>();

        Adjust();

        //adjust layers
        Renderers[2].sortingOrder
            = Renderers[3].sortingOrder
            = Renderers[4].sortingOrder
            = TopBackLayer;
        TopBackLayer += 2;

        Masks[2].frontSortingOrder
            = Renderers[2].sortingOrder + 1;
        Masks[2].backSortingOrder 
            = Renderers[2].sortingOrder;
    }

    public override void Set()
    {
        base.Set();

        Renderers[0].sprite
            = Masks[0].sprite
            = Settings.World.Row[1];
        Renderers[4].sprite
            = Settings.World.Coatings[2];
    }

    public byte Enable()
    {
        SetPlatform(Random(-8, 8) * .5f);
        //под новый вид платформ переделать (Unfinished)
        TargetJoint.enabled
                = Collider.enabled
                = true;

        //set Sprite and Angle for BackStick
        Renderers[2].sprite = Settings.World.Branches[Random(0, Settings.World.Branches.Length)];
        Masks[2].sprite = Renderers[2].sprite;
        Renderers[2].transform.eulerAngles = new Vector(0, 0, Random(-45, 45));
        int scale = UnityEngine.Random.value > .5f ? 1 : -1;
        Renderers[2].transform.localScale = new Vector(scale, scale, 1);

        //set Active, Pos and Angle for CoatingsBack
        if (Renderers[3].enabled = Random(0, 100) < (CoatingChance + Settings.Theme * 2 * CoatingChance))
            Pos(Renderers[3].transform, 10, 4);

        //set Active, Pos and Angle for CrackCoating
        if (Renderers[4].enabled = (Row.Crack && Random(0, 100) < (Settings.World.CrackChancePerLVL * Settings.LVL)))
        {
            Pos(Renderers[4].transform, 10, 4);
            Masks[1].enabled = Row.Crack = false;
            return 0;
        }

        return RowReturn();
    }

    #region CrackCoating
    void FixedUpdate()
    {
        if (Renderers[4].enabled && (transform.position.x - Camera.main.transform.position.x < -15 || transform.position.x - Camera.main.transform.position.x > 15))
            gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision target)
    {
        if (Renderers[4].enabled && target.gameObject.tag == Const.CharacterTag)
        {
            Crack.transform.position = Vector.Lerp(Renderers[2].transform.position, Renderers[2].transform.GetChild(2).position, .5f);
            Crack.transform.eulerAngles = new Vector(0, 0, Random(0, 360));
            Crack.SetActive(
                !(TargetJoint.enabled
                = Collider.enabled
                = false));
            time += .5f;
            VibratorWrapper.Vibrate(300L);
            StartCoroutine(CrackOff(Crack));
        }
    }

    System.Collections.IEnumerator CrackOff(GameObj gameObj)
    {
        while (time > 0)
        {
            yield return new UnityEngine.WaitForSeconds(.1f);
            time -= .1f;
        }
        gameObj.SetActive(false);
        time = 0;
    }
    #endregion
}