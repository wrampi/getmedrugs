﻿using UnityEngine;
using Vector = UnityEngine.Vector3;

public class RowKeeper : MonoBehaviour
{
    public GameObject Leaf, Hero, Row;

    Row[] RowList;
    sbyte index = 0, Y = 7;
    public static bool Reset;
    public static sbyte angle;
    public static readonly sbyte H = 20;
    Vector Shift, Next = Vector.zero;

    void Start()
    {
        Stick.Crack = transform.GetChild(0).gameObject;
        RowList = new Row[Y];
        Y *= 6;
    }

    void Build()//Unfinished LeafPool, косяк с построением платформ
    {
        Reset = false;
        index = 0;
        Next = Vector.zero;
        Shift.x = angle = 0;
        Shift.y = H;

        foreach (Row item in RowList)
            item?.Reconstruct();
    }

    void FixedUpdate()
    {
        if (Reset)
            Build();

        while (Hero.transform.position.y + Y > Next.y)
        {
            if (RowList[index] == null)
                RowList[index] = Instantiate(Row, transform).GetComponent<Row>();
            RowList[index].transform.position = Next;
            if (Next.y != 0)
            {
                angle = (sbyte)Random.Range(-Settings.World.angleRow, Settings.World.angleRow + 1);
                float bufTrig = Mathf.Sin(Mathf.Deg2Rad * angle);
                Shift.x = -H * .5f * bufTrig;
                bufTrig = Mathf.Cos(Mathf.Deg2Rad * angle);
                Shift.y = H * .5f * (bufTrig - 1);
                RowList[index].transform.position += Shift;

                Shift.x *= 2;
                Shift.y = H * bufTrig;
            }
            RowList[index].Enable();
            if (++index == RowList.Length)
                index = 0;
            Next += Shift;
        }                
    }
}