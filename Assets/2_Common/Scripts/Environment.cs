﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour
{
    void Awake()
    {
        FalloutAwake();
        WeatherAwake();        
        WindAwake();
        PredatorAwake();

        Set();
    }

    public void Set()
    {
        FalloutSet();
        WeatherSet();        
        WindSet();
        PredatorSet();
    }

    #region Weather Module

    ParticleSystem WeatherPS;
    public static float NowIntensity = 0;

    void WeatherAwake()
    {
        WeatherPS = transform.GetChild(0).GetComponent<ParticleSystem>();
        StartCoroutine("WeatherControl");
    }

    void WeatherSet()
    {
        WeatherPS.GetComponent<Renderer>().material = Settings.World.Weather[Settings.Theme];
        if (Settings.Theme == 0)
        {//Rain
            var main = WeatherPS.main;
            var constant = main.startSizeX;
            constant.constantMin = 1;
            constant.constantMax = 1;
            main.startSizeX = constant;
            constant = main.startSizeY;
            constant.constantMin = 1;
            constant.constantMax = 20;
            main.startSizeY = constant;
            var noise = WeatherPS.noise;
            noise.enabled = false;
        }
        else if (Settings.Theme == 1)
        {//Snow
            var main = WeatherPS.main;
            var constant = main.startSizeX;
            constant.constantMin = .4f;
            constant.constantMax = .4f;
            main.startSizeX = constant;
            constant = main.startSizeY;
            constant.constantMin = .4f;
            constant.constantMax = .4f;
            main.startSizeY = constant;
            var noise = WeatherPS.noise;
            noise.enabled = true;
        }        
    }

    IEnumerator WeatherControl()
    {
        while (true)
        {
            if (Random.Range(0, 100) < Settings.World.MainChance + Settings.LVL)
                yield return StartCoroutine("IntensityControl");
            yield return new WaitForSeconds(Random.Range(50 - Settings.LVL, 60));
        }
    }

    IEnumerator IntensityControl()
    {
        if(!WeatherPS.isPlaying)
            WeatherPS.Play();

        int Duration = Random.Range(60, 120);
        float Now = 0;
        int EndDeploy = Random.Range(2, Duration);
        int BeginDeploy = Random.Range(1, EndDeploy);

        float AddIntensity = 30 / BeginDeploy,
            MinusIntensity = 30 / (EndDeploy - Duration);

        var FallEmission = WeatherPS.emission;
        FallEmission.rateOverTime = NowIntensity = 20;

        StartCoroutine("FalloutControl");

        while (Duration > Now)
        {
            yield return new WaitForSeconds(.5f);

            if (Now < BeginDeploy)
            {
                FallEmission.rateOverTime = FallEmission.rateOverTime.constant + AddIntensity;
                NowIntensity = FallEmission.rateOverTime.constant;
            }

            if (Now >= EndDeploy)
            {
                FallEmission.rateOverTime = FallEmission.rateOverTime.constant + MinusIntensity;
                NowIntensity = FallEmission.rateOverTime.constant;
            }

            Now += .5f;
        }

        if (WeatherPS.isPlaying)
            WeatherPS.Stop();
        NowIntensity = 0;
    }
    #endregion

    #region Fallout Module

    Rigidbody2D RB2_Fallout;

    void FalloutAwake()
    {
        RB2_Fallout = transform.GetChild(1).GetComponent<Rigidbody2D>();
        RB2_Fallout.gameObject.SetActive(false);
    }

    void FalloutSet()
    {
        RB2_Fallout.gameObject.GetComponent<SpriteRenderer>().sprite = Settings.World.Fallouts[Settings.Theme];        
    }

    IEnumerator FalloutControl()
    {
        while (NowIntensity >= 20)
        {
            if (!RB2_Fallout.gameObject.activeSelf)
            {
                float WaitTime = 0;
                int Time = Random.Range(10, 30);
                while (WaitTime++ < (Time - NowIntensity * .5f))
                    yield return new WaitForSeconds(1);

                RB2_Fallout.transform.localPosition = new Vector3(Random.Range(-6f, 6f), 25, 10);
                RB2_Fallout.gameObject.SetActive(true);
                RB2_Fallout.velocity = new Vector3(Random.Range(-2f, 2f), -10, 0);
            }
            else
                while (RB2_Fallout.gameObject.activeSelf)
                {
                    if (RB2_Fallout.transform.position.y + 30 < Camera.main.transform.position.y)
                        RB2_Fallout.gameObject.SetActive(false);
                    yield return new WaitForSeconds(1);
                }
        }
    }    
    #endregion

    #region Wind Module

    ParticleSystem PS_Wind;

    void WindAwake()
    {
        PS_Wind = transform.GetChild(2).GetComponent<ParticleSystem>();
        PS_Wind.gameObject.SetActive(false);
        StartCoroutine("WindControl");
    }

    void WindSet()
    {
        PS_Wind.GetComponent<Renderer>().material = Settings.World.Weather[2];        
    }

    IEnumerator WindControl()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(50 - Settings.LVL, 65 - Settings.LVL));
            if (!PredatorActive && Random.Range(0, 100) < Settings.World.MainChance + Settings.LVL)
                yield return StartCoroutine(ShiftOfWind(Random.Range(0, 360), Random.Range(1, 3)));
        }
    }

    IEnumerator ShiftOfWind(int angle, int speed)
    {
        PS_Wind.gameObject.transform.eulerAngles = new Vector3(0, 0, angle);
        Vector3 move = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
        PS_Wind.transform.localPosition = new Vector3(-move.x * 30, -move.y * 30, -4);

        var main = PS_Wind.main;
        main.startRotation = (90 - angle) * Mathf.Deg2Rad;

        PS_Wind.gameObject.SetActive(true);

        while (PS_Wind.transform.localPosition.x < 40 &&
            PS_Wind.transform.localPosition.x > -40 &&
            PS_Wind.transform.localPosition.y < 40 &&
            PS_Wind.transform.localPosition.y > -40)
        {
            PS_Wind.transform.localPosition += move * .5f * speed;
            yield return null;
        }

        PS_Wind.gameObject.SetActive(false);
    }
    #endregion

    #region Predator Module

    SpriteRenderer[] PredatorSprites;
    SpriteRenderer FrontBody;
    public static bool PredatorActive = false;

    void PredatorAwake()
    {
        PredatorSprites = transform.GetChild(3).GetComponentsInChildren<SpriteRenderer>();
        FrontBody = PS_Wind.gameObject.GetComponentInChildren<SpriteRenderer>(true);
        FrontBody.enabled = false;
        PredatorSprites[0].gameObject.SetActive(false);
        StartCoroutine("PredatorControl");
    }

    void PredatorSet()
    {
        PredatorSprites[0].sprite = Settings.World.Bird[0];
        PredatorSprites[1].sprite = Settings.World.Bird[1];
        PredatorSprites[2].sprite = Settings.World.Bird[1];
    }

    IEnumerator PredatorControl()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(60 - Settings.LVL, 60));
            //yield return new WaitForSeconds(Random.Range(1, 2));

            if (Random.Range(0, 100) < Settings.World.MainChance + Settings.LVL)
                yield return StartCoroutine(ShiftPredator());
        }
    }

    IEnumerator ShiftPredator()
    {
        PredatorSprites[0].gameObject.SetActive(PredatorActive = true);
        PredatorSprites[0].transform.localScale = new Vector3(Random.value > .5f ? 1 : -1, 1, 1);
        PredatorSprites[0].transform.localPosition = new Vector3(-20 * PredatorSprites[0].transform.localScale.x, Random.Range(0, 16), 10);

        bool WingDown = true;
        Vector3 angle = new Vector3(0, 0, -70);
        Vector3 speed = Vector3.right * PredatorSprites[0].transform.localScale.x * Random.Range(1f, 2f) * .3f;
        while (PredatorSprites[0].transform.localPosition.x < 25 &&
            PredatorSprites[0].transform.localPosition.x > -25)
        {
            PredatorSprites[0].transform.localPosition += speed;
            if (WingDown)
            {
                if ((angle.x += 10) > 160)
                    WingDown = false;
            }
            else
                if ((angle.x -= 10) < 10)
                WingDown = true;

            PredatorSprites[1].transform.localEulerAngles = PredatorSprites[2].transform.localEulerAngles = angle;
            yield return null;
        }

        yield return new WaitForSeconds(1);
        FrontBody.enabled = true;

        //NoKill
        FrontBody.sprite = Settings.World.Bird[2];
        yield return StartCoroutine(ShiftOfWind(90, 3));

        //Kill
        FrontBody.sprite = Settings.World.Bird[3];
        FrontBody.flipX = PredatorSprites[0].transform.localScale.x > 0 ? true : false;
        yield return StartCoroutine(ShiftOfWind(270 - (int)PredatorSprites[0].transform.localScale.x * 45, 3));

        PredatorSprites[0].gameObject.SetActive(PredatorActive = FrontBody.enabled = false);
    }
    #endregion
}