﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    SpriteRenderer BGSprite;

    List<SpriteRenderer> Clouds = new List<SpriteRenderer>();
    List<Vector3> CloudsShift = new List<Vector3>();
    List<int> NextDestroy = new List<int>();

    float YCamera = 0;
    float ShiftAtCamera;

    void Awake()
    {
        BGSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();

        Vector3 Buffer = new Vector2(Random.Range(-6f, 6f), 0);

        while (Clouds.Count < 10)//World refresh!!!!!!!!!!!Unfinished
        {
            Clouds.Add(Instantiate(BGSprite, transform));
            Clouds[Clouds.Count - 1].transform.localPosition = Buffer;
            Clouds[Clouds.Count - 1].sprite = Settings.World.GetCloud();
            CloudsShift.Add(new Vector3(Random.Range(-.01f, .01f), Random.Range(-.02f, -.01f)));
            Buffer = new Vector2(Random.Range(-6f, 6f), Clouds[Clouds.Count - 1].transform.position.y + Random.Range(1, 10));
        }
        StartCoroutine(BackgroundController());
    }

    void BackgroundSet()
    {
        for (int i = 0; i < Clouds.Count; i++)
            Clouds[i].sprite = Settings.World.GetCloud();
    }

    IEnumerator BackgroundController()
    {
        while (true)//NOT POOL!!!!!!!!!!!!!!!!!!!!!!!!Unfinished
        {
            if (YCamera > Camera.main.transform.position.y)
                ShiftAtCamera = .01f;
            else
                ShiftAtCamera = -.001f;
            YCamera = Camera.main.transform.position.y;

            bool Need = true;

            for (int i = 0; i < Clouds.Count; i++)
            {
                Vector3 pos = Clouds[i].transform.position;
                pos += CloudsShift[i];
                pos.y += ShiftAtCamera;

                Clouds[i].transform.position = Vector3.Lerp(Clouds[i].transform.position, pos, 1f);

                if (Clouds[i].transform.localPosition.y > 20)
                    Need = false;

                if (Clouds[i].transform.localPosition.y < -40 || Clouds[i].transform.localPosition.x < -15 || Clouds[i].transform.localPosition.x > 15)
                    NextDestroy.Add(i);
            }

            if (Need)
            {
                Clouds.Add(Instantiate(BGSprite, transform));
                Clouds[Clouds.Count - 1].transform.localPosition = new Vector2(Random.Range(-6f, 6f), 20 + Random.Range(1, 10));
                Clouds[Clouds.Count - 1].sprite = Settings.World.GetCloud();
                CloudsShift.Add(new Vector3(Random.Range(-.01f, .01f), Random.Range(-.02f, -.01f)));
            }

            if (NextDestroy.Count > 0)
                foreach (var i in NextDestroy)
                {
                    Destroy(Clouds[i].gameObject);
                    Clouds.RemoveAt(i);
                    CloudsShift.RemoveAt(i);
                }

            NextDestroy.Clear();

            yield return null;
        }
    }
}