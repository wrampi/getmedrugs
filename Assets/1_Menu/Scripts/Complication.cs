﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Complication : MonoBehaviour
{
    Text[] buttons;
    int UpValue;

    void Awake()
    {
        buttons = GetComponentsInChildren<Text>(true);
    }

    void OnEnable()
    {
        UpValue = Settings.Stats[6];
        for (int i = 0; i < Settings.Stats.Length; i++)
        {
            buttons[i].text = Settings.Stats[i].ToString();
            if (i < 6)
                UpValue -= Settings.Stats[i];
        }
        buttons[8].text = UpValue.ToString();
    }

    public void Click(int i)
    {
        if (UpValue > 0 && Settings.Stats[i] < 10)
        {
            buttons[8].text = (--UpValue).ToString();
            buttons[i].text = (++Settings.Stats[i]).ToString();
        }
    }

    public void ClickUp(int i)
    {
            Settings.Stats[i]++;
            buttons[i].text = Settings.Stats[i].ToString();
    }

    void OnDisable()
    {
        Settings.Save();

        Settings.Jump = Settings.Stats[0];
        Settings.LinkCount = Settings.Stats[1];
        Settings.RopeCast = Settings.Stats[2];
        Settings.StunTime = Settings.Stats[3];
        Settings.Acceleration = Settings.Stats[4];
        Settings.Light = Settings.Stats[5];
        Settings.LVL = Settings.Stats[6];
        Settings.Bees = Settings.Stats[7];
    }
}
