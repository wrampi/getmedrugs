﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Menu : MonoBehaviour
{
    public Character Hero;

    void OnEnable()
    {
        if (Settings.Bees > Settings.LVL)
        {
            Settings.Stats[6] = ++Settings.LVL;
            Settings.Stats[7] = Settings.Bees = 0;

            Settings.Save();
        }
    }

    public void StartGame() => Hero.Rope.GoRope(Vector2.left + Vector2.up);
}