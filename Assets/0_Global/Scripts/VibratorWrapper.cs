﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VibratorWrapper
{
    static AndroidJavaObject vibrator = null;

    static VibratorWrapper()
    {

        var unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        var unityPlayerActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
#if UNITY_ANDROID && !UNITY_EDITOR
        vibrator = unityPlayerActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
#endif
    }

    public static bool HasVibrator()
    {
        return vibrator.Call<bool>("hasVibrator");
    }

    public static void Cancel()
    {
        if (HasVibrator()) vibrator.Call("cancel");
    }

    public static void Vibrate(long time)
    {
        if (Settings.GetVibro() && HasVibrator()) vibrator.Call("vibrate", time);
    }
}