﻿using UnityEngine;

public class FPS : MonoBehaviour
{
    private readonly int w = Screen.width,
        h = Screen.height;
    float deltaTime = 0;
    const string displayAcc = "{0} FPS",
        display = "{0:0.0} ms ({1:0.} fps)";

    GUIStyle style = new GUIStyle();
    Rect rect;

    void Start()
    {
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 3 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);

        rect = new Rect(w * .02f, h * .01f, w, style.fontSize);
    }

    void Update() => deltaTime += Time.deltaTime - deltaTime;

    void OnGUI()
    {
        float msec = deltaTime * 100;
        float fps = 1 / deltaTime;
        GUI.Label(rect, string.Format(display, msec, fps), style);
    }
}