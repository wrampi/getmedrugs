﻿using System.IO;
using UnityEngine;

public class Settings : BaseScript
{
    //
    private AudioSource Preview;

    //PlayerPrefs
    private static int _playerSettings;
    private static void SetSettings(int value)
    {
        _playerSettings ^= value;
        PlayerPrefs.SetInt(Const.Settings, _playerSettings);
    }

    //Параметры
    public static int Jump, LinkCount, RopeCast, StunTime, Acceleration, Light, LVL, Bees;
    public static int[] Stats = new int[8];

    //Звук
    private static bool _sound = false;
    public static bool GetSound() => _sound;
    public void SetSound()
    {
        _sound = !_sound;
        SetSettings(Const.SoundFlag);
    }

    //Вибро
    private static bool _vibro = false;
    public static bool GetVibro() => _vibro;
    public void SetVibro()
    {
        _vibro = !_vibro;
        SetSettings(Const.VibroFlag);
    }

    //Выбранный мир
    private static byte _worldIndex;
    private static byte WorldIndex
    {
        get => _worldIndex;

        set => _worldIndex = value == 3
            ? (byte)0
            : value == byte.MaxValue
                ? (byte)2
                : value;
    }
    public static WorldSettings World;
    public WorldSettings[] worlds;

    //Хз
    public GameObject MenuGO, Rule, Environment;    
    public static sbyte Way;
    public static byte Volume, Theme = 0;
    public static bool CatcherHero = false, Game = true;
    
    void Awake()
    {
        CameraTF = Camera.main.transform;

        if (!PlayerPrefs.HasKey(Const.Settings))
            SetSettings(Const.FirstPlayFlags);
        //0-1 Current World, 2-5 Volume, 6 Sound, 7 Vibro, 8 First Play

        _playerSettings = PlayerPrefs.GetInt(Const.Settings);
        _sound = Get1Bit(6);
        _vibro = Get1Bit(7);
        _worldIndex = Get2Bit();

        Volume = Get4Bit(2);

        Preview = GetComponent<AudioSource>();

        Set();
        SaveLoadAwake();
    }

    void Start() => DontDestroyOnLoad(this);

    void Update()
    {
        if (GetSound() && !Preview.isPlaying)
        {
            Preview.Play();
            Preview.time = 4;
        }

        if (!GetSound() && Preview.isPlaying)
            Preview.Stop();

        if (!Game)
        {
            CameraTarget.Reset = true;
            Set();
            MenuGO.gameObject.SetActive(true);
            Rule.gameObject.SetActive(false);
            Environment.gameObject.SetActive(false);
        }
    }

    void Set()
    {
        Game = true;

        World = worlds[WorldIndex];
        switch (WorldIndex)
        {
            case 1:
                CameraTF.eulerAngles = Const.Vector3_180;
                Way = -1;
                break;
            default:
                CameraTF.eulerAngles = Const.Vector3_Zero;
                Way = 1;
                break;
        }

        Load();
        RowKeeper.Reset = true;
    }

    #region SlideWorld
    public void Click(int i)
    {
        WorldIndex += (byte)i;
        _playerSettings = 0b111111100 & _playerSettings | WorldIndex;
        PlayerPrefs.SetInt(Const.Settings, _playerSettings);
        Set();
    }
    #endregion
    #region SaveLoad

    private static string path, dir;

    void SaveLoadAwake()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        dir = Application.persistentDataPath + Const.PathSaveTag;
#else
        dir = Application.dataPath + Const.PathSave;
#endif
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);
    }

    public static void Load()
    {
        path = dir + WorldIndex + Const.SaveEnd;

        if (!File.Exists(path))
        {
            using (var stream = File.Create(path)) 
            {
                stream.WriteByte(0);
                stream.WriteByte(0);
                stream.WriteByte(0);
                stream.WriteByte(0);
                stream.WriteByte(0);
            }
        }
        else
        {
            using (var stream = File.OpenRead(path))
            {
                int read = stream.ReadByte();
                Jump = Stats[0] = 15 & read;
                LinkCount = Stats[1] = 15 & read >> 4;

                read = stream.ReadByte();
                RopeCast = Stats[2] = 15 & read;
                StunTime = Stats[3] = 15 & read >> 4;

                read = stream.ReadByte();
                Acceleration = Stats[4] = 15 & read;
                Light = Stats[5] = 15 & read >> 4;

                LVL = Stats[6] = stream.ReadByte();

                Bees = Stats[7] = stream.ReadByte();
            }
        }
    }

    public static void Save()
    {
        path = dir + WorldIndex + Const.SaveEnd;

        using (var stream = File.OpenWrite(path))
        {
            stream.WriteByte((byte)(Stats[0] | Stats[1] << 4));
            stream.WriteByte((byte)(Stats[2] | Stats[3] << 4));
            stream.WriteByte((byte)(Stats[4] | Stats[5] << 4));
            stream.WriteByte((byte)Stats[6]);
            stream.WriteByte((byte)Stats[7]);
        }
    }
    #endregion
    #region GetBits
    private static bool Get1Bit(byte pos = 0)
        => (0b1 & _playerSettings >> pos) == 1;
    private static byte Get2Bit(byte pos = 0)
        => (byte)(0b11 & _playerSettings >> pos);
    private static byte Get4Bit(byte pos = 0)
        => (byte)(0b1111 & _playerSettings >> pos);
    #endregion
}