﻿using Boo.Lang;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Pool<T>
    where T : Component
{
    private readonly Queue<int> notused;
    private readonly T[] _list;
    private readonly T _template;
    private Vector2 _startPos;

    public Pool(int count, T templateObject, Vector2 startPos)
    {
        _template = templateObject;
        _list = new T[count];
        _startPos = startPos;

        for (int i = 0; i < count; i++)
            notused.Enqueue(i);
    }

    public void Add(Transform transform)
    {
        if (notused.Count != 0)
        {
            var index = notused.Dequeue();

            if (_list[index] is null)
                _list[index] = Object.Instantiate(_template, transform);
            else
                _list[index].gameObject.SetActive(true);

            _list[index].transform.localPosition = _startPos;
        }
    }

    public void Delete(int index)
    {
        _list[index].gameObject.SetActive(false);
        notused.Enqueue(index);
    }
}