﻿using UnityEngine;
using UnityEditor;
using System;

public class Core
{
    [Flags]
    public enum MenuEnum 
    {
        SpiderWorld = 0b000001,
        JellyWorld  = 0b000010,
        AllienWorld = 0b000011,
        Volume      = 0b000100, 
        Sound       = 0b001000, 
        Vibro       = 0b010000, 
        FirstPlay   = 0b100000 
    }
}