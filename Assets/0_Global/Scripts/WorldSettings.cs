﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSettings : MonoBehaviour
{
    [Header("Fallout")]
    public Sprite RainSprite;
    public Sprite SnowSprite;
    [Header("Weather")]
    public int MainChance;
    public Material[] Weather;
    public Sprite[] Clouds;
    [Header("Row")]
    public byte angleRow;
    public byte PairOfSticks;
    public float CrackChancePerLVL, HangedChancePerLVL;
    public int LeafChance;
    public Sprite[] Row;
    public Sprite[] Neares;
    public Sprite[] Branches;
    public Sprite[] Coatings;
    public Sprite[] GlueSkin;
    public Sprite[] IceSkin;
    public Sprite[] Leaves;
    [Header("Hero")]
    public int Jump;
    public int LinkCount, RopeCast, Acceleration, Light;
    public AudioClip JumpOGG, FalloutOGG, GlueOGG, IceOGG;
    public Sprite BodyNude;
    public RuntimeAnimatorController BodyAnim;
    [Header("Bird")]
    public Sprite[] Bird;

    public AudioClip[] HangedsOGG=new AudioClip[2];
    public Sprite[][] Hangeds = new Sprite[2][];

    //
    public Sprite[] Fallouts = new Sprite[2];
    void Awake()
    {
        Fallouts[0] = RainSprite;
        Fallouts[1] = SnowSprite;

        HangedsOGG[0] = GlueOGG;
        HangedsOGG[1] = IceOGG;

        Hangeds[0] = new Sprite[GlueSkin.Length];
        Hangeds[1] = new Sprite[GlueSkin.Length];
        
        for (int i = 0; i < GlueSkin.Length; i++)
        {
            Hangeds[0][i] = GlueSkin[i];
            Hangeds[1][i] = IceSkin[i];
        }
    }

    public Sprite GetCloud() => Clouds[Random.Range(0, Clouds.Length)];
}