﻿using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    public Transform Target;
    public static float x = 0;
    public static bool Reset = false;

    void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep; //?????
        //QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(x, Target.position.y + 5, -10), .05f);
        if(Reset)
        {
            enabled = Reset = false;
            transform.position = new Vector3(0, -2.5f, -10);
        }
    }
}