﻿using UnityEngine;

class Const
{
    public static byte Zero { get; set; } = 0;
    public static byte One { get; set; } = 1;
    public static byte AnimNo { get; set; } = 0;
    public static byte AnimJump { get; set; } = 1;
    public static byte AnimJumpOfWall { get; set; } = 2;
    public static byte TreshHoldVelocity { get; set; } = 3;

    public static string PlatformTag { get; set; } = "Platform";
    public static string WallTag { get; set; } = "Wall";
    public static string HangedTag { get; set; } = "Hanged";
    public static string DangerTag { get; set; } = "Dangers";
    public static string CharacterTag { get; set; } = "Character";
    public static string Settings { get; set; } = "Settings";
    public static string PathSave { get; set; } = "/Saves/";
    public static string SaveEnd { get; set; } = ".sdw";

    public static Vector3 Vector3_Zero { get; set; } = new Vector3(0, 0, 0);
    public static Vector3 Vector3_180 { get; set; } = new Vector3(0, 0, 180);

    public static int FirstPlayFlags { get; set; } = 0b111101000;
    public static int SoundFlag { get; set; } = 0b01000000;
    public static int VibroFlag { get; set; } = 0b10000000;
}
